package com.sornambigai.biz.billgeneration.Contoller.billmanagement;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sornambigai.biz.billgeneration.service.billmanagement.BillManagementService;
import com.sornambigai.biz.billgeneration.service.billmanagement.model.BillCalculationRequestModel;
import com.sornambigai.biz.billgeneration.service.billmanagement.model.BillCalculationResponseModel;
import com.sornambigai.biz.billgeneration.service.billmanagement.model.FileResponseModel;
import com.sornambigai.entity.BillHeaderEntity;
import com.sornambigai.entity.BillStatus;

@RestController
@RequestMapping(value = "auth")
public class BillManagementController {

	@Autowired
	private BillManagementService billManagementService;

	@GetMapping(value = "/getBillsByStatus")
	public List<BillHeaderEntity> getBillsByStatus(@RequestBody(required = true) BillStatus billStatus) {
		return billManagementService.getBillsByStatus(billStatus);
	}

	@GetMapping(value = "/getBillById")
	public BillHeaderEntity getBillById(@RequestParam(required = false) String billId) {
		return billManagementService.getBillByBillId(billId);
	}

	@PostMapping(value = "/insertBill")
	@ResponseBody
	public BillHeaderEntity insertBill(@RequestBody BillHeaderEntity billRequestModel) {
		return billManagementService.insertBill(billRequestModel);
	}

	@PostMapping(value = "/deleteByBillId")
	public boolean deleteBillById(@RequestParam(required = true) String billId) {
		return billManagementService.deleteBill(billId);
	}

	@PostMapping(value = "/calculateForSingleItem")
	public BillCalculationResponseModel calculateBillForSingleItem(
			@RequestBody BillCalculationRequestModel requestModel) {
		return billManagementService.calculateBillForSingleItem(requestModel);
	}

	@GetMapping(value = "/searchBills")
	public List<BillHeaderEntity> searchBills(@RequestParam(required = false) String keyword,
			@RequestParam(required = false) long offset) {
		return billManagementService.searchBills(keyword, offset);
	}

	@PostMapping(value = "/closeAndPrintBill")
	public FileResponseModel closeAndPrintBill(@RequestBody BillHeaderEntity billRequestModel) {
		return billManagementService.closeAndPrintBill(billRequestModel);
	}

	@GetMapping(value = "/printBill")
	public FileResponseModel printBill(@RequestParam(required = true) String billId) {
		return billManagementService.printBill(billId);
	}
}
