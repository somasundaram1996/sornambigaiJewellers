package com.sornambigai.biz.billgeneration.service.billmanagement.report;

import java.util.Map;

import com.sornambigai.biz.billgeneration.service.billmanagement.model.FileResponseModel;
import com.sornambigai.entity.BillHeaderEntity;

public interface ReportService {

	FileResponseModel exportBill(BillHeaderEntity billModel, Map<String, String> itemCategoryNameMap);

}
