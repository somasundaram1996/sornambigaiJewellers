package com.sornambigai.biz.billgeneration.service.usercheck.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
@AllArgsConstructor
public class ChangePasswordResponseModel {

	private final boolean result;
	private final String message;
}
