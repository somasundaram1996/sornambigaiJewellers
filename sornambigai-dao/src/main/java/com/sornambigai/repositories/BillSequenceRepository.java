package com.sornambigai.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sornambigai.dto.BillSequenceDto;

public interface BillSequenceRepository extends JpaRepository<BillSequenceDto, Long> {

}
