package com.sornambigai.repositories;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.sornambigai.dto.UsersDto;

public interface UsersRepository extends JpaRepository<UsersDto, String> {
	@Query(value = "select * from users where email_id = ?1 ", nativeQuery = true)
	public UsersDto get(String emailId, String userName);

	@Query(value = "SELECT * FROM users WHERE email_id=?1 AND role='ADMIN'", nativeQuery = true)
	public UsersDto getAdminUser(String emailId);

	@Query(value = "SELECT * FROM users WHERE role='ADMIN'", nativeQuery = true)
	public UsersDto checkIfAdminAlreadyExist();

	@Transactional
	@Modifying
	@Query(value = "UPDATE users SET password=?2 WHERE email_id=?1", nativeQuery = true)
	public void updateUserPassword(String emailId, String password);

}
