package com.sornambigai.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.sornambigai.dto.ItemCategoryMstDto;

public interface ItemCategoryMstRepository extends JpaRepository<ItemCategoryMstDto, Integer> {

	@Query(value="SELECT * FROM item_category_mst WHERE item_category_id=?1", nativeQuery = true)
	public ItemCategoryMstDto getByItemCategoryId(String itemCategoryId);
}
