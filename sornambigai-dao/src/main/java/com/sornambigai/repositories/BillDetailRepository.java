package com.sornambigai.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sornambigai.dto.BillDetailDto;

public interface BillDetailRepository extends JpaRepository<BillDetailDto, Long>{

}
