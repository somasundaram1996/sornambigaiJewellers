package com.sornambigai.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sornambigai.dto.BillHeaderDto;

public interface BillHeaderRepository extends JpaRepository<BillHeaderDto, Long> {

	@Query(value = "SELECT * FROM bill_header WHERE bill_header.bill_status =?1", nativeQuery = true)
	public List<BillHeaderDto> getBillsByStatus(int billStatus);

	@Query(value = "SELECT * FROM bill_header WHERE bill_id=?1", nativeQuery = true)
	public BillHeaderDto getExistingBill(String billId);

	@Query(value = "SELECT * FROM bill_header WHERE bill_id like %:keyword% OR customer_name LIKE %:keyword% ORDER BY bill_header_id LIMIT 20 OFFSET :offset", nativeQuery = true)
	public List<BillHeaderDto> searchBills(@Param("keyword") String keyWord, @Param("offset") long offset);

}
