package com.sornambigai.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@ToString
public enum BillStatus {

	OPEN(1), DRAFT(2), CLOSED(3);

	private int statusKey;

	public static BillStatus of(int billStausValue) {
		BillStatus[] billStatuses = BillStatus.values();
		BillStatus billStatus = null;
		for (int index = 0; index < billStatuses.length; index++) {
			if (billStatuses[index].getStatusKey() == billStausValue) {
				billStatus = billStatuses[index];
			}
		}
		return billStatus;
	}

}
