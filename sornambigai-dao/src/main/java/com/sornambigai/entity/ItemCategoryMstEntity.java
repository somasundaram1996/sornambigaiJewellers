package com.sornambigai.entity;

import javax.persistence.Entity;

import com.sornambigai.dto.ItemCategoryMstDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class ItemCategoryMstEntity {

	private int itemCategoryMstId;

	private String itemCategoryId;

	private String itemCategoryName;
	
	private BillCalculationType billCalculationType;

	public static ItemCategoryMstEntity formEntity(ItemCategoryMstDto dto) {
		ItemCategoryMstEntity entity = new ItemCategoryMstEntity();
		entity.setItemCategoryMstId(dto.getItemCategoryMstId());
		entity.setItemCategoryId(dto.getItemCategoryId());
		entity.setItemCategoryName(dto.getItemCategoryName());
		entity.setBillCalculationType(BillCalculationType.of(dto.getBillCalculationType()));
		return entity;
	}
	
	public static ItemCategoryMstDto formDto(ItemCategoryMstEntity entity) {
		ItemCategoryMstDto dto = new ItemCategoryMstDto();
		dto.setItemCategoryMstId(dto.getItemCategoryMstId());
		dto.setItemCategoryId(entity.getItemCategoryId());
		dto.setItemCategoryName(entity.getItemCategoryName());
		dto.setBillCalculationType(entity.getBillCalculationType().getBillCalculationType());
		return dto;
	}
}
