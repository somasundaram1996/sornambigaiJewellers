package com.sornambigai.dao;

import java.util.List;

import com.sornambigai.entity.BillDetailEntity;

public interface BillDetailDao {

	boolean deleteItemsForExistingBill(List<BillDetailEntity> itemsToBeDeleted);
}
