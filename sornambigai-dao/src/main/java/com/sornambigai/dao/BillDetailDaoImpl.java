package com.sornambigai.dao;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import com.sornambigai.entity.BillDetailEntity;
import com.sornambigai.repositories.BillDetailRepository;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class BillDetailDaoImpl implements BillDetailDao {

	private final BillDetailRepository billDetailRepository;

	@Override
	public boolean deleteItemsForExistingBill(List<BillDetailEntity> itemsToBeDeleted) {
		try {
			billDetailRepository
					.deleteAll(itemsToBeDeleted.stream().map(BillDetailEntity::formDto).collect(Collectors.toList()));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
